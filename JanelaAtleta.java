import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JScrollPane;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList;
import java.util.List;
import model.Atleta;
import javax.swing.SwingConstants;


public class JanelaAtleta extends JInternalFrame {
	private JTextField textNome;
	private JTextField textIdade;
	private JTextField textPeso;
	private JTextField textEndereco;
	private JTable table;
	
	List<Atleta> lista = new ArrayList<Atleta>();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JanelaAtleta frame = new JanelaAtleta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JanelaAtleta() {
		setBounds(100, 100, 668, 455);
		
		JPanel painelDeBotoes = new JPanel();
		
		JPanel painelDeRegistro = new JPanel();
		
		final JScrollPane scrollPane = new JScrollPane();
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(painelDeRegistro, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(painelDeBotoes, GroupLayout.PREFERRED_SIZE, 646, Short.MAX_VALUE)
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 646, Short.MAX_VALUE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(painelDeBotoes, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(painelDeRegistro, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nome", "Idade", "Peso", "Endere\u00E7o"
			}
		));
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel = new JLabel("Nome do(a) Atleta");
		
		textNome = new JTextField();
		textNome.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("");
		
		JLabel lblNewLabel_2 = new JLabel("Idade");
		
		textIdade = new JTextField();
		textIdade.setColumns(10);
		
		JLabel lblPeso = new JLabel("Peso");
		
		textPeso = new JTextField();
		textPeso.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Endereço");
		
		textEndereco = new JTextField();
		textEndereco.setColumns(10);
		GroupLayout gl_painelDeRegistro = new GroupLayout(painelDeRegistro);
		gl_painelDeRegistro.setHorizontalGroup(
			gl_painelDeRegistro.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_painelDeRegistro.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_painelDeRegistro.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel)
						.addComponent(lblNewLabel_1)
						.addComponent(lblNewLabel_2)
						.addComponent(lblNewLabel_3))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_painelDeRegistro.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_painelDeRegistro.createSequentialGroup()
							.addComponent(textIdade, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblPeso)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(textPeso, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(textNome, GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE)
						.addComponent(textEndereco))
					.addContainerGap(201, Short.MAX_VALUE))
		);
		gl_painelDeRegistro.setVerticalGroup(
			gl_painelDeRegistro.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_painelDeRegistro.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_painelDeRegistro.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(textNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_painelDeRegistro.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_1)
						.addGroup(gl_painelDeRegistro.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblNewLabel_2)
							.addComponent(textIdade, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblPeso)
							.addComponent(textPeso, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_painelDeRegistro.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_3)
						.addComponent(textEndereco, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(79, Short.MAX_VALUE))
		);
		painelDeRegistro.setLayout(gl_painelDeRegistro);
		
		JButton btnNovo = new JButton("Novo");
		btnNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Atleta obj = new Atleta();
				obj.setNome(textNome.getText());
				obj.setIdade(textIdade.getText());
				obj.setPeso(textPeso.getText());
				
				lista.add(obj);
			}
		});
		
		JButton btnNewButton = new JButton("Gravar");
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Atleta obj2 = new Atleta();
				obj2.setNome(scrollPane.getToolkit());
			}
		});
		
		JButton btnExcluir = new JButton("Excluir");
		
		JButton btnCarregar = new JButton("Carregar Tabela");
		GroupLayout gl_painelDeBotoes = new GroupLayout(painelDeBotoes);
		gl_painelDeBotoes.setHorizontalGroup(
			gl_painelDeBotoes.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_painelDeBotoes.createSequentialGroup()
					.addGap(69)
					.addComponent(btnNovo)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton)
					.addGap(18)
					.addComponent(btnBuscar)
					.addGap(18)
					.addComponent(btnExcluir)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnCarregar)
					.addContainerGap(68, Short.MAX_VALUE))
		);
		gl_painelDeBotoes.setVerticalGroup(
			gl_painelDeBotoes.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_painelDeBotoes.createSequentialGroup()
					.addGap(19)
					.addGroup(gl_painelDeBotoes.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton)
						.addComponent(btnBuscar)
						.addComponent(btnExcluir)
						.addComponent(btnCarregar)
						.addComponent(btnNovo))
					.addContainerGap(18, Short.MAX_VALUE))
		);
		painelDeBotoes.setLayout(gl_painelDeBotoes);
		getContentPane().setLayout(groupLayout);

	}
}
